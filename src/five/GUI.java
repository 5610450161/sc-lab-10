package five;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class GUI {
	private JFrame frame;
	private JButton redButton;
	private JButton greenButton;
	private JButton blueButton;
	private JMenuBar menubar;
	private JMenu menuColor;
	private JMenuItem red;
	private JMenuItem green;
	private JMenuItem blue;
	private JPanel panel;
	private JPanel background;
	
	public GUI(){
		createFrame();
		

		
	}
	
	public void createFrame(){
		frame = new JFrame();
		createPanel();
		frame.setJMenuBar(createMenu());
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createPanel(){
		panel = new JPanel();
		background = new JPanel();
	}
	
	public JMenuBar createMenu(){
		menubar = new JMenuBar();
		menuColor = new JMenu("Color");
		red = new JMenuItem("RED Color");
		green = new JMenuItem("GREEN Color");
		blue = new JMenuItem("BLUE Color");
		
		red.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				background.setBackground(Color.RED);
			}
			
		});
		
		green.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				background.setBackground(Color.GREEN);
			}
			
		});
		
		blue.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				background.setBackground(Color.BLUE);
			}
			
		});
		
		menuColor.add(red);
		menuColor.add(green);
		menuColor.add(blue);
		menubar.add(menuColor);
		return menubar;
	}
}
