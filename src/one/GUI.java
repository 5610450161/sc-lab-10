package one;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	private JFrame frame;
	private JButton redButton;
	private JButton greenButton;
	private JButton blueButton;
	private JPanel panel;
	private JPanel background;
	
	public GUI(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createPanel(){
		panel = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createButton();
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		background.add(panel, BorderLayout.SOUTH);
	}
	
	public void createButton(){
		redButton = new JButton("RED");
		greenButton = new JButton("GREEN");
		blueButton = new JButton("Blue");
		redButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.RED);
			}
			
		});
		
		greenButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.GREEN);
			}
			
		});
		
		blueButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.BLUE);
			}
			
		});
	}
}
