package three;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	private JFrame frame;
	private JCheckBox redButton;
	private JCheckBox greenButton;
	private JCheckBox blueButton;
	private JPanel panel;
	private JPanel background;
	
	public GUI(){
		createFrame();
		
	}
	
	public void createFrame(){
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createPanel(){
		panel = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createCheckButton();
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		background.add(panel, BorderLayout.SOUTH);
	}
	
	public void createCheckButton(){
		redButton = new JCheckBox("RED");
		redButton.setSelected(false);
		greenButton = new JCheckBox("GREEN");
		greenButton.setSelected(false);
		blueButton = new JCheckBox("Blue");
		blueButton.setSelected(false);
		ActionListener listener = new AddInterestListener();
		redButton.addActionListener(listener);
		greenButton.addActionListener(listener);
		blueButton.addActionListener(listener);
	}

	
	class AddInterestListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			
			if(redButton.isSelected() && greenButton.isSelected() && blueButton.isSelected()){
				background.setBackground(Color.BLACK);
			}else if(redButton.isSelected() && greenButton.isSelected()){
				background.setBackground(Color.YELLOW);
			}else if(redButton.isSelected() && blueButton.isSelected()){
				background.setBackground(Color.PINK);
			}else if(blueButton.isSelected() && greenButton.isSelected()){
				background.setBackground(Color.ORANGE);
			}else if(redButton.isSelected()){
				background.setBackground(Color.RED);
			}else if(greenButton.isSelected()){
				background.setBackground(Color.GREEN);
			}else if(blueButton.isSelected()){
				background.setBackground(Color.BLUE);
			}else{
				background.setBackground(null);
			}
			
		}

	}
}
