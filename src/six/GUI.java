package six;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI {
	private JFrame frame;
	private JButton depositButton;
	private JButton withdrawButton;
	private JButton checkButton;
	private JPanel panel;
	private JPanel panel2;
	private JPanel background;
	private JTextField inputbalance;
	private JLabel show;
	
	public GUI(){
		createFrame();
		//createPanel();
		//createButton();
		
	}
	
	public void createFrame(){
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createPanel(){
		panel = new JPanel();
		panel2 = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		
		
		depositButton = new JButton("Deposit");
		withdrawButton = new JButton("Withdraw");
		checkButton = new JButton("CheckBalance");
		
		show = new JLabel("Show Result");
		inputbalance =  new JTextField(20);
		
		panel.add(depositButton);
		panel.add(withdrawButton);
		panel.add(checkButton);
		panel2.add(inputbalance);
		background.add(panel, BorderLayout.EAST);
		background.add(panel2, BorderLayout.WEST);
		background.add(show, BorderLayout.SOUTH);
	}
	

	
	public void createButton(){

		depositButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.RED);
			}
			
		});
		
		withdrawButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.GREEN);
			}
			
		});
		
		checkButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.BLUE);
			}
			
		});
	}
}
