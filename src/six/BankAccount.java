package six;

public class BankAccount {
	private int balance;
	public BankAccount(int balance){
		this.balance = balance;
	}
	
	public void deposit(int balance){
		this.balance = this.balance + balance;
	}
	
	public void withdraw(int balance){
		this.balance = this.balance - balance;
	}
	
	public int checkBalance(){
		return this.balance;
	}
	
}
