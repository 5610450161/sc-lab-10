package four;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	private JFrame frame;
	private JButton redButton;
	private JButton greenButton;
	private JButton blueButton;
	private JPanel panel;
	private JPanel background;
	private JComboBox comboColor;
	private String[] color = {"Choose Color","RED", "GREEN", "BLUE"};
	public GUI(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void createPanel(){
		panel = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createComboBox();
		panel.add(comboColor);
		background.add(panel, BorderLayout.SOUTH);
	}
	
	public void createComboBox(){
		comboColor = new JComboBox(color);
		comboColor.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(comboColor.getSelectedItem().equals("RED")){
					background.setBackground(Color.RED);
				}
				
				else if(comboColor.getSelectedItem().equals("GREEN")){
					background.setBackground(Color.GREEN);
				}
				
				else if(comboColor.getSelectedItem().equals("BLUE")){
					background.setBackground(Color.BLUE);
				}
				
				else{
					background.setBackground(null);
				}
			}	
		});	
	}
}
